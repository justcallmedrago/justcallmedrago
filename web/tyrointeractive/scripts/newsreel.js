$(function () {
	controlNewsreel($("#newsreel > .nrTitle"), $("#newsreel > .nrImg"));
	controlMarquee($("#tyro_navbar > .marquee"), $("#tyro_navbar > .marquee > .text > p"));
});

function controlNewsreel(titles, imgs) {
	var curPost = titles.length,
		timeoutRef,
		enterPost,
		leavePost,
		delay = 5000;
		
	var log = (function () {
		var myLog = $("#log");
		
		return function (msg) {
			myLog.append("<p>" + msg + "</p>");
		}
	})();
	
	enterPost = function (myNum) {
		
		return function () {
			clearTimeout(timeoutRef);
			
			switchPost(myNum);
			curPost = myNum;
		};
	}
	
	leavePost = function (myNum) {
		
		return function () {
			timeoutRef = setTimeout(runNewsreel, delay);
		};
	}
	
	function switchPost(pNum) {
		//log("switching " + curPost + " for " + pNum);
		var hideCallback = function (postToHide) {
			return function() {
				imgs.eq(postToHide).hide();
				//log("hiding " + postToHide);
			};
		};
		
		titles.eq(curPost).removeClass("active");
		titles.eq(pNum).addClass("active");
		
		//imgs.eq(pNum).show();
		//imgs.eq(curPost).hide();
		
		imgs.eq(pNum).stop(false, true);
		imgs.eq(curPost).stop(false, true);
		
		if (pNum == curPost) {
			imgs.eq(pNum).show();
		} else if (pNum < curPost) {
			imgs.eq(pNum).show();
			imgs.eq(curPost).fadeOut(delay/4);
		} else {
			imgs.eq(pNum).fadeIn(delay/4, hideCallback(curPost));
		}

		curPost = pNum;
	}
	
	function runNewsreel() {
		//log("running reel for " + curPost);
		
		if (curPost + 1 >= titles.length) {
			switchPost(0);
		} else {
			switchPost(curPost + 1);
		}

		timeoutRef = setTimeout(runNewsreel, delay);
	}
	
	titles.each(function (index) {
		$(this).mouseenter(enterPost(index)).mouseleave(leavePost(index));
	});
	
	imgs.hide();
	
	runNewsreel();
}

function controlMarquee(marqueeElem, pArray) {
	var defaultMargin = +pArray.eq(0).css("margin-left").replace('px', ''),
		setNextP,
		curP;
	//alert(pArray.length + " " + pArray.eq(0).width());
	//alert("+=" + (pArray.eq(0).css("margin-left") + pArray.eq(0).width()));
	
	setNextP = (function () {
		var curNum = -1;
		
		return function () {
			if (curNum >= pArray.length - 1) {
				curNum = 0;
			} else {
				curNum++;
			}
			
			curP = pArray.eq(curNum);
		};
	}());
	
	function enterMarquee() {
		curP.stop();
	}
	
	function leaveMarquee() {
		runMarquee(true);
	}
	
	function moveToBack() {
		pArray.parent().append(curP.css("margin-left", defaultMargin).detach());
		
		setNextP();
	}
	
	function finishedAnim() {
		moveToBack();
		runMarquee();
	}
	
	function runMarquee(wasStopped) {
		var startMargin = wasStopped ? +curP.css("margin-left").replace('px', '') : defaultMargin,
			pWidth = curP.width(),
			duration = (startMargin + pWidth) / 0.04; // px / (px/ms)
		
		curP.animate({
			"margin-left": "-=" + (startMargin + pWidth)
		},duration,'linear', finishedAnim);
	}
	
	marqueeElem.mouseenter(enterMarquee).mouseleave(leaveMarquee);

	setNextP();
	runMarquee();
}









