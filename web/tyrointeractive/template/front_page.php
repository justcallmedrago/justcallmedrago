<?php
	function newsreel() {
		
		$numPosts = 8;
		$args['category'] = get_cat_id("Front Page");
		$args['numberposts'] = $numPosts;
		
		$fpPosts = get_posts($args);
		
		foreach ($fpPosts as $post) {
			$custom = get_post_custom($post->ID);
			if (isset($custom['f_img'][0])) {
				$img_urls[] = $custom['f_img'][0];
				$post_titles[] = $post->post_title;
				$post_links[] = $post->guid;
			}
		}
		
		$numPosts = count($img_urls);

		for ($i = 0; $i < $numPosts; $i += 1):
			$style = "background-image: url(" . $img_urls[$i] . "); " . (($i == 0) ? "" : "display: none;");
		
		?>
			<div class="nrImg" style="<?php echo $style; ?>"></div>
		<?php
		
		endfor;
		for ($i = 0; $i < $numPosts; $i += 1):
			
		?>
			<div class='nrTitle'>
				<div class='solid'></div>
				<div class='gradient'></div>
				<p>
					<a href='<?php echo $post_links[$i]; ?>'><?php echo $post_titles[$i]; ?></a>
				</p>
			</div>
			<br>
		<?php
		
		endfor;
	}
	
	function marquee() {
		$marqueePost = get_page_by_title("Marquee");
		echo $marqueePost->post_content;
	}
?>
