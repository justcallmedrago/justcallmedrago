<!DOCTYPE html>

<?php
	require('./template/front_page.php');
?>

<html>
	<head>
		<title>Tyro Interactive - Home</title>
		<?php wp_head(); ?>
		<link rel="stylesheet" type="text/css" href="/css/style.css" />
		<link rel="stylesheet" type="text/css" href="/css/titlebox.css" />
		
	</head>

	<body>
		<div id="tyro_header">
			<div id="logo">
				<a href="/">
					<img src="/img/logo2.png" alt="Tyro Interactive Logo">
				</a>
			</div>
		</div>
		<div id="tyro_navbar">
			<div id="page_gradient"></div>
			<div class="t_button">
				<div class="left"></div>
				<div class="middle"></div>
				<div class="right"></div>
				<div class="text">
					<p><a href="/">Home</a></p>
				</div>
			</div>
			<div class="t_button">
				<div class="left"></div>
				<div class="middle"></div>
				<div class="right"></div>
				<div class="text">
					<p><a href="/projects">Projects</a></p>
				</div>
			</div>
			<div class="t_button">
				<div class="left"></div>
				<div class="middle"></div>
				<div class="right"></div>
				<div class="text">
					<p><a href="/blog">Blog</a></p>
				</div>
			</div>
			<div class="marquee">
				<div class="left"></div>
				<div class="middle"></div>
				<div class="text">
					<?php marquee(); ?>
				</div>
			</div>
		</div>
		<div id="page">
			<div id="newsreel" class="titlebox">
				<div class="titlebox_top_left"></div>
				<div class="titlebox_top_right"></div>
				<div class="titlebox_right"></div>
				<div class="titlebox_bottom_left"></div>
				<div class="titlebox_bottom_right"></div>
				<div class="titlebox_bottom"></div>
				
				<?php newsreel(); ?>
			</div>
			<div id="tyro_content">
				<div class="top_left"></div>
				<div class="top_right"></div>
				<div class="left"></div>
				<div class="right"></div>
				<h1>Most Recent Blog Posts</h1>
				<hr>
				<?php recentPosts(); ?>
			</div>
			<div id="tyro_footer">
				<div class="left"></div>
				<div class="right"></div>
				<hr>
				<p class="text">
					&copy;2012 Tyro Interactive &bull; <a href="/">Home</a>
				</p>
			</div>
		</div>
		
		<!--div id="log"></div-->
		<script src="/scripts/jquery-1.7.2.min.js"></script>
		<script src="/scripts/newsreel.js"></script>
	</body>
</html>
