<div id="tyro_navbar">
	<div id="page_gradient"></div>
	<div class="t_button">
		<div class="left"></div>
		<div class="middle"></div>
		<div class="right"></div>
		<div class="text">
			<p><a href="/">Home</a></p>
		</div>
	</div>
	<div class="t_button">
		<div class="left"></div>
		<div class="middle"></div>
		<div class="right"></div>
		<div class="text">
			<p><a href="/projects">Projects</a></p>
		</div>
	</div>
	<div class="t_button">
		<div class="left"></div>
		<div class="middle"></div>
		<div class="right"></div>
		<div class="text">
			<p><a href="/blog">Blog</a></p>
		</div>
	</div>
	<div class="marquee">
		<div class="left"></div>
		<div class="middle"></div>
		<div class="text">
			<?php marquee(); ?>
		</div>
	</div>
</div>