(function($) {
	$(function() {
	    $('#slideshow_2').cycle({
	        fx: 'fade',		
			speed:  900, 
			timeout: 5000, 
			pager: '.ss2_wrapper .slideshow_paging', 
	        prev: '.ss2_wrapper .slideshow_prev',
	        next: '.ss2_wrapper .slideshow_next',
			before: function(currSlideElement, nextSlideElement) {
			}
	    });
	
		$('.ss2_wrapper').mouseenter(function(){
			$('#slideshow_2').cycle('pause');
			$('.ss2_wrapper .slideshow_prev').stop(true, true).animate({ left:'20px'}, 200);
			$('.ss2_wrapper .slideshow_next').stop(true, true).animate({ right:'20px'}, 200);
	    }).mouseleave(function(){
			$('#slideshow_2').cycle('resume');
			$('.ss2_wrapper .slideshow_prev').stop(true, true).animate({ left:'-45px'}, 200);
			$('.ss2_wrapper .slideshow_next').stop(true, true).animate({ right:'-45px'}, 200);
	    });
	
	// ---------------------------------------------------
		
		$('a[href="#"]').click(function(event){ 
			event.preventDefault(); // for this demo disable all links that point to "#"
		});
		
	});
})($);