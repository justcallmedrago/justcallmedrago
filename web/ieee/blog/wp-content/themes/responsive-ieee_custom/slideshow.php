<?php
	// Exit if accessed directly
	if ( !defined('ABSPATH')) exit;
?>

		<div class="ss2_wrapper">
		
			<a href="#" class="slideshow_prev"><span>Previous</span></a>
			<a href="#" class="slideshow_next"><span>Next</span></a>
				
			<div class="slideshow_paging"></div>
			
			<div class="slideshow_box">
				<div class="data"></div>
			</div>
			
			<div id="slideshow_2" class="slideshow">
				<div class="slideshow_item">
					<div class="image"><a href="#"><img src="/img/1.jpg" width="960" height="300" /></a></div>
				</div>
								
				<div class="slideshow_item">
					<div class="image"><a href="#"><img src="/img/2.jpg" width="960" height="300" /></a></div>
				</div>
				
				<div class="slideshow_item">
					<div class="image"><a href="#"><img src="/img/3.jpg" width="960" height="300" /></a></div>
				</div>

				<div class="slideshow_item">
					<div class="image"><a href="#"><img src="/img/4.jpg" width="960" height="300" /></a></div>
				</div>
				
				<div class="slideshow_item">
					<div class="image"><a href="#"><img src="/img/5.jpg" width="960" height="300" /></a></div>
				</div>
			</div>

		</div><!-- .ss2_wrapper -->
		