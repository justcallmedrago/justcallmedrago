<?php

// Exit if accessed directly
if ( !defined('ABSPATH')) exit;

/**
 * Header Template
 *
 *
 * @file           header.php
 * @package        Responsive 
 * @author         Emil Uzelac 
 * @copyright      2003 - 2013 ThemeID
 * @license        license.txt
 * @version        Release: 1.3
 * @filesource     wp-content/themes/responsive/header.php
 * @link           http://codex.wordpress.org/Theme_Development#Document_Head_.28header.php.29
 * @since          available since Release 1.0
 */
?>
<!doctype html>
<!--[if !IE]>      <html class="no-js non-ie" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>    <html class="no-js ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>    <html class="no-js ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>    <html class="no-js ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>

<meta charset="<?php bloginfo('charset'); ?>" />
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">

<title><?php wp_title('&#124;', true, 'right'); ?></title>

<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<?php wp_head(); ?>
</head>

<?php if (defined("NO_ORANGE_STRIPE")) : ?>
	<body <?php body_class("no_orange_stripe"); ?>>
<?php else: ?>
	<body <?php body_class("has_orange_stripe"); ?>>
<?php endif; ?>

<div id="stripe_full_container">
	<div id="stripe_960_container">
		<div id="orange_left"></div>
		<div id="orange_logo">
			<a href="<?php echo home_url('/'); ?>"></a>
		</div>
		<div id="orange_mid"></div>
		<div id="orange_right"></div>
		<div id="blue_stripe_container">
			<div id="blue_top_static"></div>
			<div id="blue_mid_static"></div>
			<div id="blue_bottom_static"></div>
		</div>
	</div>
	<div id="blue_scale_container">
		<div id="blue_top_scale"></div>
		<div id="blue_mid_scale"></div>
		<div id="blue_bottom_scale"></div>
	</div>
</div>

<?php responsive_container(); // before container hook ?>
<div id="container" class="hfeed">
    

    
    <?php responsive_header(); // before header hook ?>
    <div id="header">

		<?php responsive_header_top(); // before header content hook ?>
    
        <?php /*if (has_nav_menu('top-menu', 'responsive')) { ?>
	        <?php wp_nav_menu(array(
				    'container'       => '',
					'fallback_cb'	  =>  false,
					'menu_class'      => 'top-menu',
					'theme_location'  => 'top-menu')
					); 
				?>
        <?php } */?>
        
    <?php responsive_in_header(); // header hook ?>

    <div id="logo">
        <a href="<?php echo home_url('/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/core/images/logo.png" alt="<?php bloginfo('name'); ?>" /></a>
    </div><!-- end of #logo -->  
    
    <div id="nameplate">
	    <a href="<?php echo home_url('/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/core/images/nameplate.png" alt="<?php bloginfo('name'); ?>" /></a>
    </div>

    <?php get_sidebar('top'); ?>
				<?php wp_nav_menu(array(
				    'container'       => 'div',
						'container_class'	=> 'main-nav',
						'fallback_cb'	  =>  'responsive_fallback_menu',
						'theme_location'  => 'header-menu',
						'after' => '<div class="arrow"></div>')
					); 
				?>
                
            <?php /*if (has_nav_menu('sub-header-menu', 'responsive')) { ?>
	            <?php wp_nav_menu(array(
				    'container'       => '',
					'menu_class'      => 'sub-header-menu',
					'theme_location'  => 'sub-header-menu')
					); 
				?>
            <?php } */ ?>

			<?php responsive_header_bottom(); // after header content hook ?>
 
    </div><!-- end of #header -->
    
    <?php responsive_header_end(); // after header container hook ?>
    
	<?php responsive_wrapper(); // before wrapper container hook ?>
    <div id="wrapper" class="clearfix">
		<?php responsive_wrapper_top(); // before wrapper content hook ?>
		<?php responsive_in_wrapper(); // wrapper hook ?>