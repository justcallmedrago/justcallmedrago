(function ($) {
	$(document).ready(function () {
		var newsreel = $("#newsreel"),
			images = newsreel.children("img.nr_img"),
			descriptions = newsreel.children(".nr_desc"),
			descWidths = [],
			waitDuration = 5000,
			fadeDuration = 300,
			moveDuration = 500,
			originalRight = $(descriptions[0]).css("right"),
			numImgs = images.size(),
			curImg = 0,
			curTimer,
			i;
		
		for (i = 0; i < numImgs; i++) {
			descWidths.push($(descriptions[i]).outerWidth());
		}
			
		function inOut(toFadeIn, toFadeOut) {
			$(images[toFadeOut]).fadeOut(fadeDuration);
			$(images[toFadeIn]).fadeIn(fadeDuration);
			$(descriptions[toFadeOut]).fadeOut(fadeDuration);
			$(descriptions[toFadeIn])
				.css("right", -descWidths[toFadeIn])
				.fadeIn(fadeDuration, function () {
					$(descriptions[toFadeIn]).animate({
						right: originalRight
					}, moveDuration);
				});
		}
		
		function prevImage() {
			curImg--;
			
			if (curImg < 0) {
				curImg = numImgs - 1;
				inOut(curImg, 0);
			} else {
				inOut(curImg, curImg + 1);
			}
		}
		
		function nextImage() {
			curImg++;
			
			if (curImg >= numImgs) {
				curImg = 0;
				inOut(curImg, numImgs - 1);
			} else {
				inOut(curImg, curImg - 1);
			}
		}
		
		function readyForNext() {
			nextImage();
			curTimer = window.setTimeout(readyForNext, waitDuration + fadeDuration);
		}
		
		function leftClicked() {
			window.clearTimeout(curTimer);
			prevImage();
			curTimer = window.setTimeout(readyForNext, waitDuration*2 + fadeDuration);
		}
		
		function rightClicked() {
			window.clearTimeout(curTimer);
			nextImage();
			curTimer = window.setTimeout(readyForNext, waitDuration*2 + fadeDuration);
		}
		
		newsreel.children(".nr_left_arrow").click(leftClicked);
		newsreel.children(".nr_right_arrow").click(rightClicked);
		curTimer = window.setTimeout(readyForNext, waitDuration);
	});
}(jQuery));