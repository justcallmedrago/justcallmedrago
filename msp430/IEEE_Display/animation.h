/*
 * animation.h
 *
 *  Created on: Jun 28, 2013
 *      Author: Dragomir
 */

#ifndef ANIMATION_H_
#define ANIMATION_H_

#include "dispString.h"
#include "rowBuffer.h"

#define MAX_ANIMATIONS 10

enum AnimationType {
	NONE,
	HIDE,
	BLINK,
	MOVE_PIXELS_PER_FRAME,
	MOVE_FRAMES_PER_PIXEL
};

enum AnimationStart {
	WITH_PREVIOUS,
	AFTER_PREVIOUS
};

enum AnimType {
	STILL_ANIMATION
};

struct animPair {
	void * anim;
	AnimType type;
};

struct StillAnimData {
	dispString ds;
	position pos;
	unsigned int lengthInFrames;
	bool startWithPrev;
};

struct MovingAnimData {
	dispString ds;
	bool startWithPrev;
	position pStart, pEnd;
	offsetPair speed;
	unsigned char movementInterval; // moves every __ frames. 3 = every third frame
};

struct CrosshairAnimData {
	const position posArray[64];
	unsigned int arrLength;
	bool startWithPrev;
};

struct DotAnimData {
	const position posArray[64];
	unsigned int arrLength;
	bool startWithPrev;
};

class Animation {
public:
	virtual bool augmentDisplay(RowBuffer * buffer) const =0;
	virtual bool nextFrame()=0;
	virtual void start()=0;
	virtual bool withPrevious()=0;
};

class StillAnimation : public Animation {
	const StillAnimData * const data;
	unsigned int curFrame;

public:
	StillAnimation(const StillAnimData * dataIn);
	bool augmentDisplay(RowBuffer * buffer) const;
	bool nextFrame();
	void start();
	bool withPrevious();
};

class MovingAnimation : public Animation {
	const MovingAnimData * const data;
	position pos;
	unsigned char frameCounter;

public:
	MovingAnimation(const MovingAnimData * dataIn);
	bool augmentDisplay(RowBuffer * buffer) const;
	bool nextFrame();
	void start();
	bool withPrevious();
};

class CrosshairAnimation : public Animation {
	const CrosshairAnimData * const data;
	unsigned int curPos;

public:
	CrosshairAnimation(const CrosshairAnimData * dataIn);
	bool augmentDisplay(RowBuffer * buffer) const;
	bool nextFrame();
	void start();
	bool withPrevious();
};

class DotAnimation : public Animation {
	const DotAnimData * const data;
	unsigned int curPos;

public:
	DotAnimation(const DotAnimData * dataIn);
	bool augmentDisplay(RowBuffer * buffer) const;
	bool nextFrame();
	void start();
	bool withPrevious();
};

#endif /* ANIMATION_H_ */
