#include "msp430g2553.h"
#include "spi.h"
#include "timer.h"
#include "display_controller.h"
#include "alpha7x5.h"
#include "animation.h"

#if !defined(ARRAY_SIZE)
    #define ARRAY_SIZE(x) (sizeof((x)) / sizeof((x)[0]))
#endif

#define CIRCLE_LOC {0,0}, {1,1}, {2,2}, {3,3}, {4,4}, {5,5}, {6,6}, {7,7}
#define RANDOM_LOC {12, 10}, {11, 10}, {10, 10}, {9, 10}, {8, 10}, \
					{9, 11}, {10, 12}, {11, 13}, {12, 14}, {13, 15}, \
					{12, 16}, {13, 15}, {13, 15}, {13, 15}, {13, 15}, \
					{12, 16}, {11, 17}, {10, 18}, {11, 18}, {12, 18}, \
					{13, 18}, {14, 18}, {15, 18}, {16, 18}, {17, 18}, \
					{16, 19}, {15, 20}, {14, 21}, {13, 22}, {12, 23}, \
					{12, 22}, {12, 21}, {12, 20}, {12, 19}, {12, 18}, \
					{12, 17}, {13, 16}, {14, 15}, {15, 14}, {16, 13}, \
					{17, 12}, {16, 11}, {15, 10}, {14, 9}, {13, 8}, \
					{12, 7}, {11, 6}, {10, 5}, {9, 5}, {8, 5}, {7, 5}, \
					{6, 5}, {5, 5}

#define IEEE_DS {"ieee"}
#define NAME_DS {"DRAGO ROSSON"}
#define HELLO_DS {"HELLO"}
#define RACKSPACE_DS {"RACKSPACE"}

#define ORIGIN {0, 0}
#define ORIGIN2 {0, 8}
#define ORIGIN3 {0, 16}

#define TEST_CORNER_1 {-4, -1}
#define TEST_CORNER_2 {55, -1}
#define TEST_CORNER_3 {55, 19}
#define TEST_CORNER_4 {-4, 19}

static const StillAnimData ieee_test_data = { IEEE_DS, {5, 5}, 0xA, false };
static const MovingAnimData ieee_test_data1 = { IEEE_DS, false, TEST_CORNER_1, TEST_CORNER_2, {1, 0}, 1 };
static const MovingAnimData ieee_test_data2 = { IEEE_DS, false, TEST_CORNER_2, TEST_CORNER_3, {0, 1}, 1 };
static const MovingAnimData ieee_test_data3 = { IEEE_DS, false, TEST_CORNER_3, TEST_CORNER_4, {-1, 0}, 1 };
static const MovingAnimData ieee_test_data4 = { IEEE_DS, false, TEST_CORNER_4, TEST_CORNER_1, {0, -1}, 1 };

static const MovingAnimData NAME_ANIM_DATA = { NAME_DS, false, {0, -8}, ORIGIN, {0, 1}, 1 };
static const StillAnimData NAME_STILL_DATA = { NAME_DS, ORIGIN, 0x20, false };
static const MovingAnimData HELLO_ANIM_DATA = { HELLO_DS, true, {64, 8}, {7, 8}, {-2, 0}, 1 };
static const StillAnimData HELLO_STILL_DATA = { HELLO_DS, {7, 8}, 0x20, true };
static const MovingAnimData RACKSPACE_ANIM_DATA = { RACKSPACE_DS, true, {-50, 16}, {14, 16}, {2, 0}, 1 };
static const StillAnimData RACKSPACE_STILL_DATA = { RACKSPACE_DS, {14, 16}, 0x20, true};

static const CrosshairAnimData crossesData = { {CIRCLE_LOC}, 8, false };

int main(void)
{
	WDTCTL = WDTPW + WDTHOLD;

	P1OUT = 0;
	P1DIR = BIT6;

	if (CALBC1_16MHZ==0xFF) {	// If calibration constant erased
		while(1);				// do not load, trap CPU!!
	}
	DCOCTL = 0;
	BCSCTL1 = CALBC1_16MHZ;
	DCOCTL = CALDCO_16MHZ;

	CrosshairAnimation crosses = CrosshairAnimation(&crossesData);

	/*StillAnimation ieeeAnim = StillAnimation(&ieee_anim_data);
	StillAnimation ieeeAnim2 = StillAnimation(&ieee_anim_data2);
	StillAnimation ieeeAnim3 = StillAnimation(&ieee_anim_data3);*/

	StillAnimation ieeeTest = StillAnimation(&ieee_test_data);
	MovingAnimation ieeeTest1 = MovingAnimation(&ieee_test_data1);
	MovingAnimation ieeeTest2 = MovingAnimation(&ieee_test_data2);
	MovingAnimation ieeeTest3 = MovingAnimation(&ieee_test_data3);
	MovingAnimation ieeeTest4 = MovingAnimation(&ieee_test_data4);

	MovingAnimation nameAnim = MovingAnimation(&NAME_ANIM_DATA);
	StillAnimation nameStill = StillAnimation(&NAME_STILL_DATA);
	MovingAnimation helloAnim = MovingAnimation(&HELLO_ANIM_DATA);
	StillAnimation helloStill = StillAnimation(&HELLO_STILL_DATA);
	MovingAnimation rackspaceAnim = MovingAnimation(&RACKSPACE_ANIM_DATA);
	StillAnimation rackspaceStill = StillAnimation(&RACKSPACE_STILL_DATA);

	Animation * a[] = { &crosses,
		&nameAnim,

		&nameStill,
		&helloAnim,

		&nameStill,
		&helloStill,
		&rackspaceAnim,

		&nameStill,
		&helloStill,
		&rackspaceStill
	};

	DisplayController::beginDisplaying(a, ARRAY_SIZE(a));

	__bis_SR_register(LPM0_bits + GIE); // CPU off, enable interrupts
}
