/*
 * position.h
 *
 *  Created on: Jun 28, 2013
 *      Author: Dragomir
 */

#ifndef POSITION_H_
#define POSITION_H_

struct coordPair {
	signed int x;
	signed int y;
};

typedef coordPair position;
typedef coordPair offsetPair;

#endif /* POSITION_H_ */
