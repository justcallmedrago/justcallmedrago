/*
 * spi.cpp
 *
 * Created on: Jun 13, 2013
 * Author: Dragomir
 */
#include "spi.h"

SPI::voidCallback SPI::callback = 0;

void SPI::configure() {
	P1SEL = BIT1 + BIT2 + BIT4;
	P1SEL2 = BIT1 + BIT2 + BIT4;
	UCA0CTL0 |= UCMSB + UCCKPH + UCMST + UCSYNC;	// 3-pin, 8-bit SPI master
	UCA0CTL1 |= UCSSEL_2;							// SMCLK
	UCA0BR0 = 0x02;
	UCA0BR1 = 0x00;
	UCA0MCTL = 0;									// No modulation
	UCA0CTL1 &= ~UCSWRST;							// **Initialize USCI state machine**
	IE2 |= UCA0RXIE;								// Enable USCI0 TX interrupt
}

void SPI::transmitByte(unsigned char byte, voidCallback cb) {
	if (callback) {
		volatile bool error = true;
	}
	callback = cb;

	while (!(IFG2 & UCA0TXIFG));
	UCA0TXBUF = byte;
}

void SPI::call() {
	if (callback) {
		voidCallback toCall = callback;
		callback = 0;
		toCall();
	} else {
		volatile bool error = true;
	}
}

#pragma vector=USCIAB0RX_VECTOR
__interrupt void SPI::USCIA0RX_ISR()
{
	char throwaway = UCA0RXBUF; // prevent buffer overflow
	SPI::call();
}
