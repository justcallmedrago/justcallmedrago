/*
 * alpha7x5.h
 *
 *  Created on: Jun 18, 2013
 *      Author: Dragomir
 */

#ifndef ALPHA7X5_H_
#define ALPHA7X5_H_

#include "position.h"
#include "dispString.h"
#include "rowBuffer.h"

struct symbol7 {
	char width;
	char data [7];
};

struct symbol8 {
	char width;
	char data [8];
};

class Alpha7x5 {
	static const symbol7 symbols [3];
	static const symbol7 capitals [26];

public:
	static bool augmentDisplay(char * string, position pos, RowBuffer * buffer);
	static const symbol7* getSymbol(char c);
	static bool writeSymbolToDisplay(const symbol7* s, int posX, int posY, int row, RowBuffer * buffer);
};

#endif /* ALPHA7X5_H_ */
