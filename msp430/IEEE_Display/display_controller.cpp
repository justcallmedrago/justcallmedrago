/*
 * display_controller.cpp
 *
 *  Created on: Jun 30, 2013
 *      Author: Dragomir
 */

#include "display_controller.h"
#include "timer.h"

Animation ** DisplayController::animations = 0;
unsigned int DisplayController::animationCount = 0;
RowBuffer DisplayController::bufferA = RowBuffer(0);
RowBuffer DisplayController::bufferB = RowBuffer(1);

RowBuffer * DisplayController::writeBuffer = 0;
RowBuffer * DisplayController::displayBuffer = 0;

unsigned int DisplayController::curAnimation = 0;
unsigned int DisplayController::curFrame = 0;
unsigned char DisplayController::curScan = 0;
unsigned char DisplayController::rowToDisplay = 0;
bool DisplayController::rowReady = true;
bool DisplayController::frameReady = true;

void DisplayController::beginDisplaying(Animation ** anims, unsigned int animCount) {
	SPI::configure();
	P2OUT = 0;
	P2DIR = BIT0 + BIT1 + BIT2 + BIT3;

	displayBuffer = &bufferA;
	writeBuffer = &bufferB;

	animations = anims;
	animationCount = animCount;

	nextAnimSet();
	finishRow();
}

void DisplayController::byteSent() {
	if (displayBuffer->nextByte()) {
		SPI::transmitByte(displayBuffer->getByte(), byteSent);
	} else {
		finishRow();
	}
}

void DisplayController::finishRow() {
	rowReady = true;
	Timer::stopStopWatch();
	unsigned int time = Timer::getStopwatchCount();

	if (time < CYCLES_BETWEEN_ROWS) {
		Timer::setTimer(0, CYCLES_BETWEEN_ROWS - time, startRow);
	} else {
		Timer::setTimer(0, CYCLES_BETWEEN_ROWS, startRow);
	}

	rowToDisplay = displayBuffer->row();

	__bis_SR_register(GIE);

	if (!nextRow() && !nextScan()) {
		nextFrame();
	}
	writeRow();
}

bool DisplayController::nextAnimFrame() {
	unsigned int i = curAnimation;
	bool stillRunning = false;

	while (i < animationCount && (i == curAnimation || animations[i]->withPrevious())) {
		if (animations[i]->nextFrame()) {
			stillRunning = true;
		}
		i++;
	}

	if (!stillRunning) {
		curAnimation = i;
	}

	return stillRunning;
}

void DisplayController::nextAnimSet() {
	unsigned int i = (curAnimation < animationCount ? curAnimation : 0);

	curAnimation = i;

	// start next batch
	while (i < animationCount && (i == curAnimation || animations[i]->withPrevious())) {
		animations[i]->start();
		i++;
	}
}

void DisplayController::nextFrame() {
	curScan = 0;

	if (!nextAnimFrame()) {
		nextAnimSet();
	}
}

bool DisplayController::nextRow() {
	swapBuffers();
	return writeBuffer->advanceRow(2);
}

bool DisplayController::nextScan() {
	curScan++;
	return curScan < SCANS_BETWEEN_FRAMES;
}

void DisplayController::startRow() {
	if (!rowReady) {
		volatile bool error = true;
	}

	Timer::startStopwatch();
	rowReady = false;

	P2OUT = rowToDisplay + BIT3;
	P2OUT &= ~BIT3;

	volatile RowBuffer d = *displayBuffer;
	volatile RowBuffer w = *writeBuffer;

	SPI::transmitByte(displayBuffer->getByte(), byteSent);
}

void DisplayController::swapBuffers() {
	RowBuffer * temp = displayBuffer;
	displayBuffer = writeBuffer;
	writeBuffer = temp;
}

void DisplayController::writeRow() {
	if (!frameReady) {
		volatile bool error = true;
	}

	frameReady = false;
	writeBuffer->clear();

	for (unsigned int a = curAnimation; a < animationCount && (a == curAnimation || animations[a]->withPrevious()); a++) {
		animations[a]->augmentDisplay(writeBuffer);
	}

	frameReady = true;
}
