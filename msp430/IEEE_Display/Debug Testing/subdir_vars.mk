################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../alpha7x5.cpp \
../animation.cpp \
../dispString.cpp \
../display_controller.cpp \
../display_main.cpp \
../rowBuffer.cpp \
../spi.cpp \
../timer.cpp 

CMD_SRCS += \
../lnk_msp430g2553.cmd 

OBJS += \
./alpha7x5.obj \
./animation.obj \
./dispString.obj \
./display_controller.obj \
./display_main.obj \
./rowBuffer.obj \
./spi.obj \
./timer.obj 

CPP_DEPS += \
./alpha7x5.pp \
./animation.pp \
./dispString.pp \
./display_controller.pp \
./display_main.pp \
./rowBuffer.pp \
./spi.pp \
./timer.pp 

CPP_DEPS__QUOTED += \
"alpha7x5.pp" \
"animation.pp" \
"dispString.pp" \
"display_controller.pp" \
"display_main.pp" \
"rowBuffer.pp" \
"spi.pp" \
"timer.pp" 

OBJS__QUOTED += \
"alpha7x5.obj" \
"animation.obj" \
"dispString.obj" \
"display_controller.obj" \
"display_main.obj" \
"rowBuffer.obj" \
"spi.obj" \
"timer.obj" 

CPP_SRCS__QUOTED += \
"../alpha7x5.cpp" \
"../animation.cpp" \
"../dispString.cpp" \
"../display_controller.cpp" \
"../display_main.cpp" \
"../rowBuffer.cpp" \
"../spi.cpp" \
"../timer.cpp" 


