/*
 * animation.cpp
 *
 *  Created on: Jun 30, 2013
 *      Author: Dragomir
 */

#include "animation.h"

StillAnimation::StillAnimation(const StillAnimData * dataIn) : data(dataIn) {
	curFrame = 0;
}

bool StillAnimation::augmentDisplay(RowBuffer * buffer) const {
	Alpha7x5::augmentDisplay(data->ds.string, data->pos, buffer);
	return true;
}

bool StillAnimation::nextFrame() {
	curFrame++;
	return curFrame < data->lengthInFrames;
}

void StillAnimation::start() {
	curFrame = 0;
	return;
}

bool StillAnimation::withPrevious() {
	return data->startWithPrev;
}

MovingAnimation::MovingAnimation(const MovingAnimData * dataIn) : data(dataIn) {
	pos = data->pStart;
	frameCounter = 0;
}

bool MovingAnimation::augmentDisplay(RowBuffer * buffer) const {
	Alpha7x5::augmentDisplay(data->ds.string, pos, buffer);
	return true;
}

bool MovingAnimation::nextFrame() {
	if (pos.x == data->pEnd.x && pos.y == data->pEnd.y) {
		return false;
	}

	if (frameCounter == data->movementInterval) {
		pos.x += data->speed.x;
		pos.y += data->speed.y;
		frameCounter = 0;

		if (data->speed.x > 0 && pos.x >= data->pEnd.x ||
				data->speed.x < 0 && pos.x <= data->pEnd.x ||
				data->speed.y > 0 && pos.y >= data->pEnd.y ||
				data->speed.y < 0 && pos.y <= data->pEnd.y) {
			pos = data->pEnd;
		}
	} else {
		frameCounter++;
	}

	return true;
}

void MovingAnimation::start() {
	pos = data->pStart;
	return;
}

bool MovingAnimation::withPrevious() {
	return data->startWithPrev;
}

CrosshairAnimation::CrosshairAnimation(const CrosshairAnimData * dataIn) : data(dataIn) {
	curPos = 0;
}

bool CrosshairAnimation::augmentDisplay(RowBuffer * buffer) const {
	if (curPos >= data->arrLength
			|| data->posArray[curPos].x < 0
			|| data->posArray[curPos].y < 0
			|| data->posArray[curPos].y >= DISPLAY_HEIGHT*8) {
		return true;
	}

	unsigned char byteX = data->posArray[curPos].x / 8;
	unsigned char byteToWrite = 0x80 >> (data->posArray[curPos].x & 0x7);

	if (byteX >= DISPLAY_WIDTH) {
		return true;
	}

	for (int y = buffer->row(); y < DISPLAY_HEIGHT*8; y+=8) {
		if (data->posArray[curPos].y != y) {
			buffer->write(byteX, y, byteToWrite);
		}
	}

	if ((data->posArray[curPos].y & 0x7) == buffer->row()) {
		for (int x = 0; x < DISPLAY_WIDTH; x++) {
			buffer->write(x, data->posArray[curPos].y, (x == byteX) ? ~byteToWrite : 0xFF);
		}
	}

	return true;
}

bool CrosshairAnimation::nextFrame() {
	curPos++;

	return curPos < data->arrLength;
}

void CrosshairAnimation::start() {
	curPos = 0;
}

bool CrosshairAnimation::withPrevious() {
	return data->startWithPrev;
}

DotAnimation::DotAnimation(const DotAnimData * dataIn) : data(dataIn) {
	curPos = 0;
}

bool DotAnimation::augmentDisplay(RowBuffer * buffer) const {
	if (curPos >= data->arrLength
			|| data->posArray[curPos].x < 0
			|| data->posArray[curPos].y < 0
			|| (data->posArray[curPos].y & 0x7) != buffer->row()) {
		return true;
	}

	unsigned char byteX = data->posArray[curPos].x / 8;
	unsigned char byteToWrite = 0x80 >> (data->posArray[curPos].x & 0x7);

	if (byteX >= DISPLAY_WIDTH) {
		return true;
	}

	buffer->write(byteX, data->posArray[curPos].y, byteToWrite);

	return true;
}

bool DotAnimation::nextFrame() {
	curPos++;

	return curPos < data->arrLength;
}

void DotAnimation::start() {
	curPos = 0;
}

bool DotAnimation::withPrevious() {
	return data->startWithPrev;
}
