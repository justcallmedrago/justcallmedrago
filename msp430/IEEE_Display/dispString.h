/*
 * dispString.h
 *
 *  Created on: Jun 30, 2013
 *      Author: Dragomir
 */

#ifndef DISPSTRING_H_
#define DISPSTRING_H_

#include "alpha7x5.h"

struct dispString {
	char * string;
};

#endif /* DISPSTRING_H_ */
