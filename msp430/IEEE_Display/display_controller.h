/*
 * display_controller.h
 *
 *  Created on: Jun 30, 2013
 *      Author: Dragomir
 */

#ifndef DISPLAY_CONTROLLER_H_
#define DISPLAY_CONTROLLER_H_

#define OVERFLOWS_BETWEEN_ROWS 0
#define CYCLES_BETWEEN_ROWS 30000
#define SCANS_BETWEEN_FRAMES 8

#include "animation.h"
#include "spi.h"
#include "position.h"
#include "rowBuffer.h"

class DisplayController {
	static RowBuffer bufferA, bufferB;
	static RowBuffer * writeBuffer, * displayBuffer;

	static Animation ** animations;
	static unsigned int animationCount;

	static unsigned int curAnimation;
	static unsigned int curFrame;
	static unsigned char curScan;
	static unsigned char rowToDisplay;
	static bool rowReady;
	static bool frameReady;

	static void byteSent();
	static void finishRow();
	static bool nextAnimFrame();
	static void nextAnimSet();
	static void nextFrame();
	static bool nextRow();
	static bool nextScan();
	static void startRow();
	static void swapBuffers();
	static void writeRow();

public:
	static void beginDisplaying(Animation ** anims, unsigned int animCount);
};

#endif /* DISPLAY_CONTROLLER_H_ */
