/*
 * timer.cpp
 *
 *  Created on: Jul 2, 2013
 *      Author: Dragomir
 */

#include "timer.h"

Timer::voidCallback Timer::callback = 0;
unsigned int Timer::overflowCount = 0;
unsigned int Timer::overflowsReq = 0;
bool Timer::stopwatchOn = false;

#pragma vector=TIMER0_A0_VECTOR
__interrupt void Timer::TIMER0_A0_ISR() {
	if (!callback) {
		volatile bool error = true;
		return;
	} else if (overflowCount < overflowsReq || stopwatchOn) {
		overflowCount++;
	} else {
		TACTL = TACLR;
		overflowCount = 0;
		voidCallback toCall = callback;
		callback = 0;
		toCall();
	}
}

unsigned int Timer::getStopwatchCount() {
	if (stopwatchOn) {
		return 0;
	} else {
		return TAR;
	}
}

unsigned int Timer::getStopwatchOverflows() {
	return overflowCount;
}

void Timer::setTimer(unsigned int ofReq, unsigned int cycles, unsigned int startCount, voidCallback cb) {
	callback = cb;
	overflowsReq = ofReq;
	overflowCount = 0;

	TACTL = TACLR;
	CCR0 = cycles;
	TACCTL0 = CCIE;
	TACTL = TASSEL_2 + MC_2;
}

void Timer::setTimer(unsigned int ofReq, unsigned int cycles, voidCallback cb) {
	setTimer(ofReq, cycles, 0, cb);
}

void Timer::startStopwatch(unsigned int startCount) {
	stopwatchOn = true;
	overflowCount = 0;
	setTimer(0, 0xFFFF, startCount, stopwatchOverflowed);
}

void Timer::startStopwatch() {
	startStopwatch(0);
}

bool Timer::stopStopWatch() {
	stopwatchOn = false;
	TACTL &= ~MC_3; // set MC_0 using MC_3 to clear MC bits (halts Timer_A)
	return overflowCount != 0;
}

void Timer::stopwatchOverflowed() {
	// do nothing. overflowcount automatically incremented.
}
