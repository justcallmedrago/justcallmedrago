################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Each subdirectory must supply rules for building sources it contributes
alpha7x5.obj: ../alpha7x5.cpp $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti3/ccsv5/tools/compiler/msp430_4.1.1/bin/cl430" -vmsp --abi=eabi -Ooff -g --include_path="C:/ti3/ccsv5/ccs_base/msp430/include" --include_path="C:/ti3/ccsv5/tools/compiler/msp430_4.1.1/include" --advice:power=all --keep_unneeded_statics --define=__MSP430G2553__ --diag_warning=225 --display_error_number --diag_wrap=off --printf_support=minimal --preproc_with_compile --preproc_dependency="alpha7x5.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

animation.obj: ../animation.cpp $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti3/ccsv5/tools/compiler/msp430_4.1.1/bin/cl430" -vmsp --abi=eabi -Ooff -g --include_path="C:/ti3/ccsv5/ccs_base/msp430/include" --include_path="C:/ti3/ccsv5/tools/compiler/msp430_4.1.1/include" --advice:power=all --keep_unneeded_statics --define=__MSP430G2553__ --diag_warning=225 --display_error_number --diag_wrap=off --printf_support=minimal --preproc_with_compile --preproc_dependency="animation.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

dispString.obj: ../dispString.cpp $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti3/ccsv5/tools/compiler/msp430_4.1.1/bin/cl430" -vmsp --abi=eabi -Ooff -g --include_path="C:/ti3/ccsv5/ccs_base/msp430/include" --include_path="C:/ti3/ccsv5/tools/compiler/msp430_4.1.1/include" --advice:power=all --keep_unneeded_statics --define=__MSP430G2553__ --diag_warning=225 --display_error_number --diag_wrap=off --printf_support=minimal --preproc_with_compile --preproc_dependency="dispString.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

display_controller.obj: ../display_controller.cpp $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti3/ccsv5/tools/compiler/msp430_4.1.1/bin/cl430" -vmsp --abi=eabi -Ooff -g --include_path="C:/ti3/ccsv5/ccs_base/msp430/include" --include_path="C:/ti3/ccsv5/tools/compiler/msp430_4.1.1/include" --advice:power=all --keep_unneeded_statics --define=__MSP430G2553__ --diag_warning=225 --display_error_number --diag_wrap=off --printf_support=minimal --preproc_with_compile --preproc_dependency="display_controller.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

display_main.obj: ../display_main.cpp $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti3/ccsv5/tools/compiler/msp430_4.1.1/bin/cl430" -vmsp --abi=eabi -Ooff -g --include_path="C:/ti3/ccsv5/ccs_base/msp430/include" --include_path="C:/ti3/ccsv5/tools/compiler/msp430_4.1.1/include" --advice:power=all --keep_unneeded_statics --define=__MSP430G2553__ --diag_warning=225 --display_error_number --diag_wrap=off --printf_support=minimal --preproc_with_compile --preproc_dependency="display_main.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

spi.obj: ../spi.cpp $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti3/ccsv5/tools/compiler/msp430_4.1.1/bin/cl430" -vmsp --abi=eabi -Ooff -g --include_path="C:/ti3/ccsv5/ccs_base/msp430/include" --include_path="C:/ti3/ccsv5/tools/compiler/msp430_4.1.1/include" --advice:power=all --keep_unneeded_statics --define=__MSP430G2553__ --diag_warning=225 --display_error_number --diag_wrap=off --printf_support=minimal --preproc_with_compile --preproc_dependency="spi.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

timer.obj: ../timer.cpp $(GEN_OPTS) $(GEN_SRCS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti3/ccsv5/tools/compiler/msp430_4.1.1/bin/cl430" -vmsp --abi=eabi -Ooff -g --include_path="C:/ti3/ccsv5/ccs_base/msp430/include" --include_path="C:/ti3/ccsv5/tools/compiler/msp430_4.1.1/include" --advice:power=all --keep_unneeded_statics --define=__MSP430G2553__ --diag_warning=225 --display_error_number --diag_wrap=off --printf_support=minimal --preproc_with_compile --preproc_dependency="timer.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '


