/*
 * spi.h
 *
 *  Created on: Jun 13, 2013
 *      Author: Dragomir
 */

#ifndef SPI_H_
#define SPI_H_

#include "msp430g2553.h"

class SPI {
	typedef void (*voidCallback)(void);

	static voidCallback callback;

	static __interrupt void USCIA0RX_ISR();
	static void call();

public:
	static void configure();
	static void transmitByte(unsigned char byte, voidCallback callback);
};

#endif /* SPI_H_ */
