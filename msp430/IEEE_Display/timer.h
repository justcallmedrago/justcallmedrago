/*
 * timer.h
 *
 *  Created on: Jul 2, 2013
 *      Author: Dragomir
 */

#ifndef TIMER_H_
#define TIMER_H_

#include "msp430g2553.h"

class Timer {
	typedef void (*voidCallback)(void);

	static voidCallback callback;
	static unsigned int overflowsReq;
	static unsigned int overflowCount;
	static bool stopwatchOn;

	static void stopwatchOverflowed();
	static __interrupt void TIMER0_A0_ISR();

public:
	static unsigned int getStopwatchCount();
	static unsigned int getStopwatchOverflows();
	static void setTimer(unsigned int ofReq, unsigned int cycles, unsigned int startCount, voidCallback cb);
	static void setTimer(unsigned int ofReq, unsigned int cycles, voidCallback cb);
	static void startStopwatch(unsigned int startCount);
	static void startStopwatch();
	static bool stopStopWatch();
};

#endif /* TIMER_H_ */
