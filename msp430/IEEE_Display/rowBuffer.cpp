/*
 * rowBuffer.cpp
 *
 *  Created on: Sep 15, 2013
 *      Author: Dragomir
 */

#include "rowBuffer.h"
#include <cstring>

#if ONEDISPLAY == 1
	const position RowBuffer::byteOrder[NUM_DISPLAYS] = { // order: {data enters --> data exits}
			{0, 0}
	};
#else
	const position RowBuffer::byteOrder[NUM_DISPLAYS] = { // order: {data enters --> data exits}
			{0, 0}, {0, 1}, {0, 2},
			{1, 2}, {1, 1}, {1, 0},
			{2, 0}, {2, 1}, {2, 2},
			{3, 2}, {3, 1}, {3, 0},
			{4, 0}, {4, 1}, {4, 2},
			{5, 2}, {5, 1}, {5, 0},
			{6, 0}, {6, 1}, {6, 2},
			{7, 2}, {7, 1}, {7, 0},
	};
#endif

RowBuffer::RowBuffer(unsigned char startRow) : curByte(NUM_DISPLAYS - 1), curRow(startRow), data() {

}

bool RowBuffer::advanceRow(unsigned int numRows) {
	curRow += numRows;
	curByte = NUM_DISPLAYS - 1;

	if (curRow < 8) {
		return true;
	} else {
		curRow &= 0x7;
		return false;
	}
}

void RowBuffer::clear() {
	std::memset(data, 0, DISPLAY_WIDTH * DISPLAY_HEIGHT);
}

unsigned char RowBuffer::getByte() {
	return data[byteOrder[curByte].y][byteOrder[curByte].x];
}

bool RowBuffer::nextByte() {
	if (curByte == 0) {
		return false;
	}

	curByte--;

	return true;
}

unsigned char RowBuffer::row() {
	return curRow;
}

bool RowBuffer::write(unsigned char x, unsigned char y, unsigned char byte) {
	if (x < DISPLAY_WIDTH && y < DISPLAY_HEIGHT*8 && (y & 0x7) == curRow) {
		data[y >> 3][x] |= byte;
		return true;
	} else {
		return false;
	}
}
