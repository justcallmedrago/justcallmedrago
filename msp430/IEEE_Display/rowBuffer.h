/*
 * rowBuffer.h
 *
 *  Created on: Sep 15, 2013
 *      Author: Dragomir
 */

#ifndef ROWBUFFER_H_
#define ROWBUFFER_H_

#include "position.h"

#define ONEDISPLAY 0

#if ONEDISPLAY == 1
	#define DISPLAY_WIDTH 1
	#define DISPLAY_HEIGHT 1
	#define NUM_DISPLAYS 1
#else
	#define DISPLAY_WIDTH 8
	#define DISPLAY_HEIGHT 3
	#define NUM_DISPLAYS 24
#endif

class RowBuffer {
	static const position byteOrder[NUM_DISPLAYS];

	unsigned char curByte;
	unsigned char curRow;
	unsigned char data[DISPLAY_HEIGHT][DISPLAY_WIDTH];

public:
	const unsigned char heightInPixels = DISPLAY_HEIGHT*8;
	const unsigned char heightInDisplays = DISPLAY_HEIGHT;
	const unsigned char widthInPixels = DISPLAY_WIDTH*8;
	const unsigned char widthInDisplays = DISPLAY_WIDTH;

	RowBuffer(unsigned char startRow);
	bool advanceRow(unsigned int numRows);
	void clear();
	unsigned char getByte();
	bool nextByte();
	unsigned char row();
	bool write(unsigned char x, unsigned char y, unsigned char byte);
};

#endif /* ROWBUFFER_H_ */
